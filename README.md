# Slide 
https://docs.google.com/presentation/d/1A_2OZNQrc92JVnFiq2EPYprluvX99RtPlHuAVevNzP0/edit#slide=id.gab8db61b22_1_0
# ChatRoom
-------------------------  
## Changes  
Jan 2nd, 2021: 
- Separate original App.js into 2 separate files, one for rendering messages and the other for rendering users currently in the room  
- Add new css for room

-------------------------
### Requirements
- Node.js
  - You can find packages in frontend/package.json (May need less because I accidentally add more than needed)
- ```django-channels, channel-redis, django-channels-presence, channels``` for websocket and ```crispy-forms``` for some css
- **redis** or some other forms to host websocket on your PC (If you don't use Redis to host on your own PC, please remember to change the settings at config/settings.py)

### Installation:
1. Install node.js packages  
   - Download Dependencies by using the command ```npm install package.json``` if you are at teamproject/frontend/, or ```npm install frontend/package.json``` when you are at teamproject/  
   - If you want to change any thing related to .js files, change it at frontend/src/components/App.js, then open terminal at frontend/ and use command ```npm run dev``` or ```npm run webpack --mode development ./src/index.js --output-path ./static/js/main.js```
2. Redis
   - For Windows users, you can download Redis [from Microsoft Archives](https://github.com/MicrosoftArchive/redis/releases). I am currently using version 3.2.100. For convenient and easy installation, download the .msi files from the website. Remember to add PATH to redis folder to environment, and follow the guides in the ```Windows Service Documentations``` file in the Redis folder
   - For Linux and MacOS users, just download using sudo or docker or anything (I am sorry, I am not used too using Linux and MacOS). The newest version of redis is 6.0+, which 
3. Django modules 
   - Aside from the usual Django-needed packages, install the other packages listed above using the commands `pip install X`, change **X** with the name of the packages listed above
   - You may get into conflicts problem, so you can add ```--use-feature=2020-resolver``` after ```pip install``` to resolve conflicts, or you can download the same version as me, which I will listed right below:
     - channels=3.0.2 
     - channels-redis=2.4.2
     - crispy-forms >= 1.10.0 
     - asgi-redis=1.1.0
     - asgiref=3.3.1
     - redis=2.10.6  
   **Note**: The above packages versions are what I found to be the most compatible with Windows 10 version 2004 and Redis 3.2.100. If you are using Linux or MacOS, there are probably newer versions of Redis that will be incompatible with the package versions above
---------------------------------
### How to host and run on your PC:
- Run redis-server on your computer
- Use command ```npm run dev``` if you have changed something in App.js, so that the changes will be reflected in main.js
  - While running a django server, if you have changed something in App.js, just re-run `npm run dev`, you don't have to restart the server
- Migrate the models and Run Django server
  


~~Will be adding some .js files to further break my own code~~  
Somewhat added a ReactJS code to render .html  

