from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Room(models.Model):
    room_name = models.CharField(max_length=30, unique=True, null=False)
    created_at = models.DateTimeField(default=timezone.now)
    is_private = models.BooleanField(default=False)
class Message(models.Model):
    sent_by=models.ForeignKey(User, related_name='messages',
        on_delete=models.CASCADE)
    text=models.TextField()
    posted_at=models.DateTimeField(default=timezone.now)
