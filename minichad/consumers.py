import json
import datetime
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from channels_presence.models import Presence, Room as PresenceRoom
from django.contrib.auth.models import User
from channels_presence.decorators import remove_presence
"""
class ChatConsumer
- Connect method is called when there is a new connection to server, i.e. someone enters some chatRoom
- disconnect is called when someone leaves a ChatRoom
- receive is called when 
"""
class ChatConsumer(WebsocketConsumer):
    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name
        UserName = self.scope["user"]
        PresenceRoom.objects.add(self.room_group_name, self.channel_name, UserName)
        # CurrentRoom = PresenceRoom.objects.get(channel_name = self.room_group_name)
        # UsersInRoom = len(Presence.objects.filter(room_id = CurrentRoom.id))
        # print(UsersInRoom)
        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )
        self.accept()
    @remove_presence
    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )
        PresenceRoom.objects.remove(self.room_group_name, self.channel_name)
        
        print("Remove user from connection")
        pass
        # CurrentRoom = PresenceRoom.objects.get(channel_name = self.room_group_name)
        # CurrentUser = User.objects.get(username = self.scope["user"])
        # print("Current User: \n{}, {}".format(CurrentUser.id, type(CurrentUser)))
        # UserId = int(CurrentUser.id)
        # Presence.objects.filter(room=self, user_id = UserId).delete()
        # if len(CurrentRoom.get_users()) == 0:
        #     CurrentRoom.prune_presences(age_in_seconds=0)
        # if len(Presence.objects.all()) == 0:
        #     PresenceRoom.objects.prune_rooms()
        # 
        # PresenceRoom.objects.remove(self.room_group_name, self.channel_name)
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        print('TextData: {}\n{}'.format(text_data_json, type(text_data)))
        if 'prune' in text_data_json:
            CurrentRoom = PresenceRoom.objects.get(channel_name = self.room_group_name)
            CurrentRoom.prune_presences(age_in_seconds = 0)
            return
        if 'newuser' in text_data_json:
            Presence.objects.touch(self.channel_name)
            # userName = text_data_json['newuser']
            CurrentRoom = PresenceRoom.objects.get(channel_name = self.room_group_name)
            UsersInRoom = CurrentRoom.get_users()
            Users = [str(user.username) for user in UsersInRoom]
            async_to_sync(self.channel_layer.group_send)(
                self.room_group_name,
                {
                    'type': 'chat_message',
                    'newuser': Users,
                }
            )
            return
        if 'disconnect' in text_data_json:
            user_name = text_data_json['disconnect']
            CurrentRoom = PresenceRoom.objects.get(channel_name = self.room_group_name)
            UsersInRoom = CurrentRoom.get_users()
            Users = [user.username for user in UsersInRoom]
            async_to_sync(self.channel_layer.group_send)(
                self.room_group_name,
                {
                    'type': 'chat_message',
                    'disconnect': user_name,
                }
            )
            return
        if 'message' in text_data_json:
            Presence.objects.touch(self.channel_name)
            # print(self.scope["user"])
            message = text_data_json['message']
            
            utcTime = datetime.datetime.now(datetime.timezone.utc)
            utcTime = utcTime.isoformat()
            # Send message to room group
            async_to_sync(self.channel_layer.group_send)(
                self.room_group_name,
                {
                    'type': 'chat_message',
                    'message': message,
                    'utcTime': utcTime,
                    'username': str(self.scope["user"])
                }
            )
            return
    def chat_message(self, event):
        print('Event: {}'.format(event))
        if 'newuser' in event:
            print("Send new user list to webSocket")
            newUser = event['newuser']
            self.send(text_data = json.dumps({
                'newuser': newUser
            }))
        if 'disconnect' in event:
            print("Remove user from connection")
            newDisconnect = event['disconnect']
            self.send(text_data = json.dumps({
                'disconnect': newDisconnect
            }))
        if 'message' in event:
            message = event['message']
            username = event['username']
            utcTime = event['utcTime']
            # Send message to WebSocket
            print("Sending something to websocket")
            self.send(text_data=json.dumps({
                'message': message,
                'username': username,
                'utcTime': utcTime,
            }))