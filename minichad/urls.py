from django.urls import path, include
from . import views
from frontend import views as frontend_views
from django.views.generic.base import TemplateView
from django.contrib.auth import views as auth_views

urlpatterns=[
    path('', frontend_views.lobby, name='lobby'),
    path('', include('django.contrib.auth.urls')),
    path('profile/', frontend_views.profile, name='profile'),
    path('profile/edit/',frontend_views.edit_profile, name='edit_profile'),
    # path('profile/password/',frontend_views.MyPasswordChangingForm.as_view(), name='change_password'),
    path('profile/password/',frontend_views.change_password,name='change_password'),
    path('register/',frontend_views.register,name='register'),
    path('about_us/',frontend_views.about_us,name='about_us'),
    path('create_room', views.create_room, name = 'create_room'),
    path('room/<str:room_name>', views.room, name = 'room'),
    path('reset_password/', auth_views.PasswordResetView.as_view(template_name='registration/password_reset.html'), name='reset_password'),
    path('reset_password_sent/', auth_views.PasswordResetDoneView.as_view(template_name='registration/password_reset_sent.html'), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='registration/password_reset_form1.html'), name='password_reset_confirm'),
    path('reset_password_complete/', auth_views.PasswordResetCompleteView.as_view(template_name='registration/password_reset_done1.html'), name='password_reset_complete'),
    
]