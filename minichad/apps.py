from django.apps import AppConfig


class MinichadConfig(AppConfig):
    name = 'minichad'
