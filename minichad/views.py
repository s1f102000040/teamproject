from frontend.views import lobby
from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404, JsonResponse
from django.utils import timezone
from minichad.models import Room
from django.contrib.auth.models import User
import re
from channels_presence.models import Presence
from channels_presence.models import Room as PresenceRoom


def create_room(request):

    if request.method == "POST":
        room_name = request.POST.get('room_name')
        room_name = re.sub(r"\s+$", "", room_name)
        room_name = re.sub(r"\s+", "_", room_name)
        room = Room(room_name=room_name,
                    created_at=timezone.now())
        room.save()
        return redirect(lobby)
    return render(request, "frontend/create_room.html")


def room(request, room_name):
    Rooms = Room.objects.all()
    if len(Rooms) == 0 and request.user and request.user.is_authenticated:
        return render(request, "frontend/create_room.html", {})
    max_user = 10
    room_list = []
    for current_room in Rooms:
        room_group_name = 'chat_' + current_room.room_name
        try:
            kek = PresenceRoom.objects.get(channel_name = room_group_name)
            # get number of users currently connected to the room
            num_users = len(kek.get_users())
            room_list.append({
                "room": current_room,
                "room_name": str(current_room.room_name),
                "num_users": num_users,
                "max_user": max_user,
                "created_at": current_room.created_at
            })
        except PresenceRoom.DoesNotExist:
            room_list.append({
                "room": current_room,
                "room_name": str(current_room.room_name),
                "num_users": 0,
                "max_user": max_user,
                "created_at": current_room.created_at
            })
    current_user = request.user.username
    context = {
        "room_name": room_name,
        "username": current_user,
        "rooms": room_list,
    }
    return render(request, "frontend/room.html", context)
