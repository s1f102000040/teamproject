from django import forms
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from django.urls import reverse_lazy

class RegisterForm(UserCreationForm):
    email = forms.EmailField(required=True)
    class Meta():
        model = User
        fields = ["username", "first_name", "last_name", "email", "password1", "password2"]
        widgets = {
            'username': forms.TextInput(attrs= {'class': 'form-control', 'placeholder': 'Write your Username'}),
            'first_name': forms.TextInput(attrs= {'class': 'form-control', 'placeholder': 'Write your first name'}),
            'last_name': forms.TextInput(attrs= {'class': 'form-control', 'placeholder': 'Write your last name'}),
            'email': forms.TextInput(attrs= {'class': 'form-control', 'placeholder': 'Write your email'}),
            'password1': forms.TextInput(attrs= {'class': 'form-control', 'placeholder': 'Password'}),
            'password2': forms.TextInput(attrs= {'class': 'form-control'})
        }
    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit = False)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']

        if commit:
            user.save()

        return user
class EditForm(UserChangeForm):
    class Meta():
        model = User
        fields = ["username", "first_name", "last_name", "email","password"]
        widgets = {
            'username': forms.TextInput(attrs= {'class': 'col-md-6 form-control form-control-alternative'}),
            'first_name': forms.TextInput(attrs= {'class': 'col-md-6 form-control', 'placeholder': 'Write your first name'}),
            'last_name': forms.TextInput(attrs= {'class': 'col-md-6 form-control', 'placeholder': 'Write your last name'}),
            'email': forms.TextInput(attrs= {'class': 'col-md-6 form-control', 'placeholder': 'Write your email'}),
            'password': forms.TextInput(attrs= {'class': 'col-md-6 form-control', 'placeholder': 'Password'}),
        }
