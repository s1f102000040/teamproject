import React, { Component } from "react";
import {render} from "react-dom";
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            loaded: false,
        }
    }
    componentDidMount() {
        // const roomName = 
        // const chatSocket = new WebSocket(
        //     'ws://'
        //     + window.location.host
        //     + '/ws/room/'
        //     + roomName
        //     + '/'
        // );
        const roomName = location.pathname.substr(1);
        const userName = JSON.parse(document.getElementById('username').textContent);
        // console.log (roomName);
        var socketPath = 'ws://'
            + window.location.host
            + '/ws/'
            + roomName 
            + '/';
        const chatSocket = new WebSocket(
            socketPath
        );
        
        // chatSocket.fa-odnoklassniki
        chatSocket.onmessage = (e) => {
            var data = JSON.parse(e.data);
            // console.table(e);
            // console.table(data);
            // if
            if('message' in data){
                console.log("Received the message");
                var message = {
                    text: data.message,
                    date: data.utcTime,
                    username: data.username,
                };
                message.date = moment(message.date).local().format('YYYY-MM-DD HH:mm:ss');
                let updated_messages = [...this.state.messages, message];
                // console.table(updated_messages);
                this.setState({
                    messages: updated_messages
                });
            }
        }
        chatSocket.onclose = (e) => {
            console.error('Chat socket closed unexpectedly');
        }
    //     document.querySelector('#chat-message-input').focus();
    //     document.querySelector('#chat-message-input').onkeyup = (e) => {
    //         if(e.keyCode === 13){
    //             document.querySelector('#chat-message-submit').click();
    //         }
    //         this.clickSubmitMessage
    //     };

    //     document.querySelector('#chat-message-submit').onclick = (e) => {
    //         var messageInputDom = document.querySelector('#chat-message-input');
    //         var message = messageInputDom.value;
    //         if(message !== ''){
    //             chatSocket.send(JSON.stringify({
    //                 'message': message,
    //             }));
    //         }
    //         messageInputDom.value = ''
    //     };
    }
    //
    render() {
        return(
            <div>
                {this.state.messages.map((item) => {
                    
                    return (
                        // <div className="row content-wrap messages">
                            <div id = "message" className = "msg">
                                <div className="media-body">
                                    <small className="pull-right time"><i className="fa fa-clock-o"></i> {item.date}</small>
                                    <h5 className="media-heading">{item.username}</h5>
                                    <small className="col-sm-11">{item.text}</small>
                                </div>
                            </div>
                        // </div>

                    );
                })}

                

            </div>
        );
    }
}

export default App;

const container = document.getElementById("app");
render (<App />, container);