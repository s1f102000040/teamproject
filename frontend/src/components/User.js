import React, { Component } from "react";
import {render} from "react-dom";
class User extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            users: [],
        }
    }
    componentDidMount() {
        // const roomName = 
        // const chatSocket = new WebSocket(
        //     'ws://'
        //     + window.location.host
        //     + '/ws/room/'
        //     + roomName
        //     + '/'
        // );
        const roomName = location.pathname.substr(1);
        const userName = JSON.parse(document.getElementById('username').textContent);
        console.log("IS THIS LOADED")
        console.log (roomName);
        var socketPath = 'ws://'
            + window.location.host
            + '/ws/'
            + roomName 
            + '/';
        const chatSocket = new WebSocket(
            socketPath
        );
        
        // chatSocket.fa-odnoklassniki
        chatSocket.onmessage = (e) => {
            var data = JSON.parse(e.data);
            // console.table(e);
            // console.table(data);
            if('newuser' in data){
                let updated_users = [...data.newuser];
                this.setState({
                    users: updated_users,
                });
            }
            if('disconnect' in data){
                let updated_users = this.state.users.filter((username) => {
                    return username !== data.disconnect;
                });
                // console.table(updated_users);
                this.setState({
                    users: updated_users,
                });
            }
        }
        chatSocket.onopen = (e) => {
            console.log("New connection");
            // console.table(e);
            chatSocket.send(JSON.stringify({
                'newuser': userName,
            }));
            
        }
        // window.addEventListener('beforeunload', (e) => {
        //     // console.table(e);
            
        // })
        chatSocket.onclose = (e) => {
            console.error('Chat socket closed unexpectedly');
        }
    }
    componentWillUnmount() {
        const roomName = location.pathname.substr(1);
        const userName = JSON.parse(document.getElementById('username').textContent);
        console.log (roomName);
        var socketPath = 'ws://'
            + window.location.host
            + '/ws/'
            + roomName 
            + '/';
        const chatSocket = new WebSocket(
            socketPath
        );
        if(this.state.users.length == 1){
            chatSocket.send(JSON.stringify({
                'prune': roomName,
            }))
        }
        chatSocket.send(JSON.stringify({
            'disconnect': userName,
        }))
    }
    render() {
        return(
            <div className="row content-wrap">
                {this.state.users.map((user) => {
                    return(
                        <div className="contact">
                            <div className="media-body">
                                <h5 className="media-heading">{user}</h5>
                            </div>
                        </div>
                    )
                })}
            </div>
                // <div class="contact">
                //     <div class="media-body">
                //         <h5 class="media-heading">Walter White</h5>
                //         <small class="pull-left time"><i>Invited</i></small>
                //     </div>
                // </div>
            
            // <div>
            //     List of Users currently in this Room: <br/>
            //     {this.state.users.map((user, index) => {
            //         return( 
            //             <div key = {index} id = "users" className = "cell large-8">
            //                 <ul>
            //                     <li>
            //                         {user}
            //                     </li>
            //                 </ul>
            //             </div>
            //         )
            //     })}

            // </div>
        );
    }
}

export default User;

const container = document.getElementById("user");
render (<User />, container);