from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404, JsonResponse
from django.utils import timezone
from minichad.models import Room
from django.contrib.auth.forms import (
    UserCreationForm, 
    AuthenticationForm, 
    UserChangeForm,
    PasswordChangeForm,
)
from django.contrib.auth.views import PasswordChangeView, PasswordResetDoneView
from django.contrib.auth import update_session_auth_hash
from .forms import RegisterForm, EditForm
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from channels_presence.models import Presence
from channels_presence.models import Room as PresenceRoom

def profile(request):
    context = {'user': request.user}
    return render(request, "frontend/profile.html", context)

def edit_profile(request):
    if(request.method == 'POST'):
        form = EditForm(request.POST, instance=request.user)
        if (form.is_valid()):
            form.save() 
            return redirect('/profile')
    else:
        form = EditForm(instance =request.user)
        context = {'form':form}
        return render(request,'frontend/edit_profile.html', context)

def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            form = PasswordChangeForm(user=request.user)
            messages.success(request, 'Your Password Was Changed Successfully... ')
        context = {'form':form}
        return render(request,'frontend/change_password.html', context)
    else:
        form = PasswordChangeForm(user=request.user)
        context = {'form':form}
        return render(request,'frontend/change_password.html', context)
        
def lobby(request):
    Rooms = Room.objects.all()
    if len(Rooms) == 0 and request.user and request.user.is_authenticated:
        return render(request, "frontend/create_room.html", {})
    max_user = 10
    room_list = []
    for current_room in Rooms:
        room_group_name = 'chat_' + current_room.room_name
        try:
            kek = PresenceRoom.objects.get(channel_name = room_group_name)
            # get number of users currently connected to the room
            num_users = len(kek.get_users())
            room_list.append({
                "room": current_room,
                "room_name": str(current_room.room_name),
                "num_users": num_users,
                "max_user": max_user,
                "created_at": current_room.created_at
            })
        except PresenceRoom.DoesNotExist:
            room_list.append({
                "room": current_room,
                "room_name": str(current_room.room_name),
                "num_users": 0,
                "max_user": max_user,
                "created_at": current_room.created_at
            })
        
        
        # user_count.append(
        #     {"room_name": len(Presence.objects.filter(room_id = kek.id).distinct())}
        # )
    print(room_list)
    context = {
        "rooms": room_list,
    }
    return render(request, "home.html", context)

def register(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = request.POST['username']
            password = request.POST['password1']
            user = authenticate(request, username=username, password=password)
            login(request, user)
            return redirect("/")
    else:
        form = RegisterForm()
    return render(request, "frontend/register.html", {"form":form})

def about_us(request):
    return render(request, "frontend/about_us.html")